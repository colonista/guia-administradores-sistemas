    Introducción

Como estudiante avanzado de la tecnicatura opte por seguir el tramo de administración de sistemas, por lo cual adquirí conocimientos que me han ayudado de manera inigualable a resolver problemas cotidianos que se presentan con frecuencia en cualquier entorno laboral y también a planificar innovaciones que se reflejaron en el incremento de la productividad en el trabajo.
Para este Trabajo Practico de Final de Carrera mi intención es presentar un problema y como contraparte la solución al mismo.

    Problemática


Desde el comienzo de la era moderna las organizaciones han buscado una forma de trabajo que permita realizar más tareas en menor tiempo y por ende menor costo (tiempo=dinero), por tal razón siempre se apuntó a metodologías de trabajo sistemáticas y ordenadas.
Con el arribo de la informática al mundo moderno innumerable cantidad de empresas comenzaron a contratar a otras que estaban especializadas en instalar y administrar los sistemas que eran adquiridos con costos en extremo elevados.
Esta metodología de trabajo sirvió mientras los únicos demandantes de administradores de sistemas eran entidades que poseían muchos recursos económicos para solventar esta forma de trabajo, pero con el pasar de los años, las empresas más pequeñas vieron que era necesario integrarse a este mundo digital o sino estaban destinadas al estancamiento y sobre todo, cediendo lugar a otras dentro del mercado.
Es por eso que muchas recurrieron a una solución poco ética y que además era ilegal, utilizar software de gestión sin abonar su correspondiente licencia de uso.
Si bien al comienzo de la era digital el concepto de software privativo no era conocido, con el correr del tiempo y con las prácticas de persecución por parte de las empresas desarrolladoras de los mismos el término fue implantándose en la cultura popular.
A causa de este accionar, las empresas y sus equipos informáticos veían que debían abonar algo que era imposible de pagar o sino volverían a la era del papel, es así como entra en juego el software libre.
El software libre siempre fue visto con desconfianza, porque la gente no entiende el concepto de compartir para un bien mejor, por tal motivo su aceptación fue a regañadientes y solo lo hacían porque el costo de aplicarlo era impresionantemente bajo, pero el problema principal es que no todas las empresas, asesores, técnicos o administradores que se dediquen al rubro informático conocen las ventajas y virtudes de este mundo libre como para salir a dar respuestas a distintos usuarios.

    Solución


Es por eso que mi Trabajo Práctico de final de carrera se centra en ayudar a estas empresas o personas, demostrando en primera medida que el software libre es tan bueno o mejor que el privativo. Brindándole otro panorama para que al tomar decisiones no siempre estén viendo una sola cara de la moneda.
Mi intención es realizar un instructivo para administradores de sistemas que no conocen el mundo libre, mostrando una puerta hacia una enorme cantidad de soluciones y fomentando a que colaboren con sus conocimientos o inquietudes dentro de esta comunidad creciente.
La idea se centra en enseñarle a preparar equipos que oficien de servidores y estaciones de trabajo, ayudándolo a implementar una estructura de red adecuada y entregando herramientas de control para detectar y evitar problemas comunes o maliciosos.
Con estos conceptos tomados la idea es continuar con los automatismos, desde copias de seguridad, tareas programadas, sistemas de monitoreo constante o mantenimiento de equipos de manera remota y despersonalizada.
También explicarle las bondades de las bases de datos libres y sus políticas de uso, para así evitar inconvenientes a la hora de realizar una elección.
Para finalizar intentaré estar siempre fomentando dentro de este manual que todo conocimiento que ellos adquieran deben compartirlo para poder entre todos ir mejorando la creciente comunidad libre, ayudándonos entre todos. 

    Estado del Arte


Para realizar mi Trabajo Final estuve evaluando distintas opciones, desde organización hasta migraciones, pero me decidí por algo que ayude a la comunidad en general, que esté disponible para quien tenga ganas de ingresar en el mundo del Software Libre y que pueda ser una guía para que no fracasen.
Es por eso que al buscar en distintos sitios web, consultar a especialistas en software libre y revisar publicaciones con las que contaba me sorprendió no encontrar nada similar a lo que pretendo realizar. Si bien hay cientos o miles de sitios que explican cómo usar distintas herramientas de gestión o como configurar los sistemas operativos, ninguno incluyó el cómo hacer una migración completa o generar desde cero una implementación que incluya todo tipos de soluciones, como así también guiar a un administrador novato o que nació en el mundo del software privativo.
Entonces por lo que hacía referencia anteriormente, hay muchos sitios que explican cómo migrar, otros como administrar redes o algunos de que software seleccionar para cada tarea por mencionar algunos. Mi misión en este trabajo es la de recopilar la mejor información, y prepararla para que sea entendible y amigable para un usuario inexperto.
Lamentablemente mi trabajo no se puede basar en el de otro y así mejorarlo pero si es posible que lo que yo realice sea la base para que alguien más lo mejore, lo adapte o lo reescriba.
Por todo lo antes dicho no me es posible referenciar a proyectos similares y no me parece correcto incluir los sitios que solo se especializan en algunas áreas a las cuales voy a nombrar en mi trabajo final.


    Objetivos
    
*Generales:* El objetivo general de este trabajo es el de dar a conocer las alternativas que presenta el mundo del software libre para todo lo que sea relacionado al trabajo de un SysAdmin y ayudar a aquellas personas que tienen interés en migrar de un entorno privativo a uno libre, dándoles una guía básica pero completa de las herramientas más comunes a utilizar en sus tareas diarias. También pretendo alcanzar a las personas que están comenzando a administrar sistemas y que no poseen ningún tipo de experiencia o capacitación. 

*Específicos:* La intención es que se pueda dar un pantallazo general a las distintas herramientas que hay en existencia y ver cuál se ajusta más a la necesidad de cada caso, comenzando por las herramientas más comunes del sistema operativo, sus comandos básicos que permiten desenvolvernos dentro del entorno. De ahí en adelante el trabajo se irá centrando en distintas áreas de la administración y siendo específico en cada una con ejemplos claros y prácticos.


    Metodologías y Cronograma

La metodología que usaré en este proyecto es la de presentar un caso con su problemática e ir resolviendo paso a paso cada inconveniente que se presente, de modo gráfico y concreto. Cada tema que se toque en el instructivo será parte de una idea general más grande, es decir, cada problema que se vaya resolviendo irá dando forma a una estructura de sistemas de una organización.
El cronograma inicial se presenta incierto en estos momentos. La idea general es que semana a semana se vaya abordando un tema a la vez y resolviendo  sus implicancias.

